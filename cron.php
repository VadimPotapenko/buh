<?php
// команда crontab -e 0 0 8 * * /путь/к/скрипту/php
// #! путь/к/интерпретатору/php

include_once 'src/crest.php';
$field = 'UF_CRM_1518701540';
//$field = 'ASSIGNED_BY_ID';
$taskName = 'Новая задача';
### получаем все компании ###
$companyTotal = CRest::call('crm.company.list', 
	array('select' => array('ID'), 'filter' => array('!'.$field => ''))
);

$iteration = intval($companyTotal['total'] / 50) + 1;
if ($iteration % 50 == 0) $iteration -= 1;
for ($i = 0; $i < $iteration; $i++) {
	$start = $i * 50;
	$companyData[] = array(
		'method' => 'crm.company.list',
		'params' => array(
			'start' => $start,
			'filter' => array(
				'!'.$field => ''
			),
			'select' => array('ID', 
				$field,
				'TITLE'
			)
		)
	);
}
if (count($companyData) > 50) $companyData = array_chunk($companyData, 50);
else $companyData = array($companyData);
for ($i = 0, $s = count($companyData); $i < $s; $i++) {
	$company[] = CRest::callBatch ($companyData[$i]);
}
writeToLog($company, 'Получены компани');

### формируем массив расчетчик => компании под его контролем ###
foreach ($company as $comp) {
	foreach ($comp['result']['result'] as $com) {
		foreach ($com as $c) {
			$arrPersonal[$c['ASSIGNED_BY_ID']][$c['ID']] = $c['TITLE'];
		}
	}
}
//writeToLog($arrPersonal, 'расчетчик -> компании');

### ищем главбуха ###
$accountant = CRest::call('department.get', array());
for ($i = 0, $s = count($accountant['result']); $i < $s; $i++) {
	if ($accountant['result'][$i]['NAME'] == 'Бухгалтерия') {
		$mainAccountant = $accountant['result'][$i]['UF_HEAD'];
	}
}

### формируем задачи расчетчикам ###
foreach ($arrPersonal as $k => $v) {
	$taskData[] = array(
		'method' => 'tasks.task.add',
		'params' => array(
			'fields' => array(
				'TITLE'          => $taskName,
				'CREATED_BY'     => $mainAccountant,
				'RESPONSIBLE_ID' => $k,
				'DEADLINE'       => date('Y-m-d', mktime(0, 0, 0, date('m'), 8, date('Y')))
			)
		)
	);
}
if (count($taskData) > 50) $taskData = array_chunk($taskData, 50);
else $taskData = array($taskData);
for ($i = 0, $s = count($taskData); $i < $s; $i++) {
	$newTask[] = CRest::callBatch($taskData[$i]);
}
writeToLog($newTask, 'Новая задача');

### формируем чек-листы к поставленным задачам ###
foreach ($newTask as $task) {
	foreach ($task['result']['result'] as $t) {
		foreach ($arrPersonal[$t['task']['responsibleId']] as $c) {
			$checkData[] = array(
				'method' => 'task.checklistitem.add',
				'params' => array(
					'ID' => $t['task']['id'],
					'fields' => array(
						'TITLE' => 'Компания: '.$c,
						'IS_COMPLETE' => 'N'
					)
				)
			);
		}
	}
}
//writeToLog($newCheck, 'массив чек-листы');
if (count($checkData) > 50) $checkData = array_chunk($checkData, 50);
else $checkData = array($checkData);
for ($i = 0, $s = count($checkData); $i < $s; $i++) {
	$newCheck[] = CRest::callBatch($checkData[$i]);
}
writeToLog($newCheck, 'чек-листы к задачам');
#######################################################################################
function writeToLog ($data, $title = 'DEBUG') {
	$log = "\n--------------------\n";
	$log .= date('d.m.Y H:i:s')."\n";
	$log .= $title."\n";
	$log .= print_r($data, 1);
	$log .= "\n--------------------\n";

	file_put_contents('debug.txt', $log, FILE_APPEND);
	return true;
}