#! /usr/bin/php
<?php
/*
* Настройка крона:
* в файле /etc/crontab  0 8 1 * * root test -f /home/bitrix/www/hook/taskraschet.php && { /usr/bin/php -f /home/bitrix/www/hook/taskraschet.php; } > /dev/null 2>&1
* #! путь/к/интерпретатору/php
*/
#================================= settings ========================================#
include_once (__DIR__.'/src/crest.php');
include_once (__DIR__.'/src/debugger/Debugger.php');
define ('RESPONSIBLE_DEFENDANT', 'UF_CRM_1518701540');     // ответственный расчетчик
define ('TASK_NAME', 'Сдача СЗВ-М');                      // имя задачи
define ('ACCOUNTANT', 'UF_CRM_1518699059');              // ответственный бухгалтер
define ('GROUP_ID', '35');
define ('DEPARTMENT', '167');                          // отдел кадров
define ('LOG_PATH', __DIR__.'/log/log.txt');          // куда пишем лог
define ('LOG_BOOL', false);                          // управление логированием
#===================================================================================#
### получаем все компании ###
$companyTotal = CRest::call ('crm.company.list', array('select' => array('ID'), 'filter' => array('!'.RESPONSIBLE_DEFENDANT => '')));
$iteration = intval($companyTotal['total'] / 50) + 1;
if ($iteraton % 50 == 0) $iteration -= 1;

for ($i = 0; $i < $iteration; $i++) {
	$start = $i * 50;
	$companyData[] = array(
		'method' => 'crm.company.list',
		'params' => array(
			'start' => $start,
			'filter' => array('!'.RESPONSIBLE_DEFENDANT => ''),
			'select' => array('ID', RESPONSIBLE_DEFENDANT, 'TITLE', 'UF_CRM_1518699059')
		)
	);
}
if (count($companyData) > 50) $companyData = array_chunk($companyData, 50);
else $companyData = array($companyData);
for ($i = 0, $s = count($companyData); $i < $s; $i ++) {
	$company[] = CRest::callBatch ($companyData[$i]);
}

### сортируем компании по алфавиту ###
usort($company[0]['result']['result'][0], function ($a, $b) {
	if ($a['TITLE'] > $b['TITLE']) return 1;
});

/*
* Если есть сотрудник отдела кадров в структуре (DEPARTMENT) (причем этот сотрудник не руководитель отдела), то берется он.
* Если есть расчетчик в компании (RESPONSIBLE_DEFENDANT), то берется он.
* Если никого нет, то берется ответственный бухгалтер. UF_CRM_1518699059 - ответственный бухгалтер
*/

### сотрудник депортамента 167 ###
$headDepartment = CRest::call ('department.get', array('ID' => DEPARTMENT))['result'][0]['UF_HEAD'];     // id главы депортамента 167
$userDepartment = CRest::call ('user.get', array('filter' => array('UF_DEPARTMENT' => DEPARTMENT, 'ACTIVE' => '1')));
foreach ($userDepartment['result'] as $value) {
	if ($value['ID'] == $headDepartment) continue;
	$user = $value['ID'];
}

### постановка задач и чек-листов ###
if (isset($user) && !empty($user)) {
	### сотрудник в депортаменте 167 - да ###
	$newTask = CRest::call ('tasks.task.add', array(
		'fields' => array(
			'TITLE'          => TASK_NAME,
			'CREATED_BY'     => $headDepartment,
			'RESPONSIBLE_ID' => $user,
			'DEADLINE'       => date('Y-m-d', mktime(0, 0, 0, date('m'), 8, date('Y'))),
			'GROUP_ID'       => GROUP_ID
		)
	));
	Debugger::writeToLog ($newTask, LOG_PATH, 'Постановка задачи сотруднику депортамента 167', LOG_BOOL);

	foreach ($company as $comp) {
		foreach ($comp['result']['result'] as $c) {
			foreach ($c as $v) {
				### чек-листы ###
				$newCheck = CRest::call ('task.checklistitem.add', array(
					'ID' => $newTask['result']['task']['id'],
					'fields' => array(
						'TITLE' => 'Компания: '.$v['TITLE'],
						'IS_COMPLETE' => 'N'
					)
				));
				Debugger::writeToLog ($newCheck, LOG_PATH, 'Постновка чек-листов в задачу', LOG_BOOL);
			}
		}
	}
} else {
	### массив для вторичных сотрудников ###
	foreach ($company as $comp) {
		foreach ($comp['result']['result'] as $c) {
			foreach ($c as $k => $v) {
				$secondUser[$v[RESPONSIBLE_DEFENDANT] ?: $v[ACCOUNTANT]][$v['ID']] = array($v['TITLE']);
			}
		}
	}
	### постановка задач для вторичных сотрудников ###
	foreach (array_keys($secondUser) as $value) {
		$newTask[] = CRest::call ('tasks.task.add', array(
			'fields' => array(
				'TITLE'          => TASK_NAME,
				'CREATED_BY'     => $headDepartment,
				'RESPONSIBLE_ID' => $value,
				'DEADLINE'       => date('Y-m-d', mktime(0, 0, 0, date('m'), 8, date('Y'))),
				'GROUP_ID'       => GROUP_ID
			)
		));
	}
	Debugger::writeToLog ($newTask, LOG_PATH, 'Постановка задачи сотруднику', LOG_BOOL);
	### чек-листы ###
	for ($i = 0, $s = count($newTask); $i < $s; $i++) {
		foreach ($newTask[$i]['result'] as $value) {
			foreach ($secondUser[$value['responsibleId']] as $k => $v) {
				$newCheck[] = CRest::call ('task.checklistitem.add', array(
					'ID'     => $value['id'],
					'fields' => array(
						'TITLE' => 'Компания: '.$v[0],
						'IS_COMPLETE' => 'N'
					)
				));
			}
		}
	}
	Debugger::writeToLog ($newCheck, LOG_PATH, 'Постaновка чек-листов в задачу', LOG_BOOL);
}