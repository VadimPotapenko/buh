<?php
/*
* Вариант скрипта, предназначенный для работы на агентах битрикс
*/
function repeat_deals () {

	### проверка даты и времени ###
	$day = date('d');
	$time = date('H');
	$file = file_get_contents(__DIR__.'/flag.txt');
	if ($day > 14 && !$file) {
		file_put_contents(__DIR__.'/flag.txt', '1');
	}

	if ($day == 1 && $time == 8 && $file) {
		include_once 'src/crest.php';

		### получаем все компании ###
		$companyTotal = CRest::call('crm.company.list', 
			array('select' => array('ID'), 'filter' => array('!UF_CRM_1518701540' => ''))
		);

		$iteration = intval($companyTotal['total'] / 50) + 1;
		if ($iteration % 50 == 0) $iteration -= 1;
		for ($i = 0; $i < $iteration; $i++) {
			$start = $i * 50;
			$companyData[] = array(
				'method' => 'crm.company.list',
				'params' => array(
					'start' => $start,
					'filter' => array(
						'!UF_CRM_1518701540' => ''
					),
					'select' => array('ID', 
						'UF_CRM_1518701540',
						'TITLE'
					)
				)
			);
		}
		if (count($companyData) > 50) $companyData = array_chunk($companyData, 50);
		else $companyData = array($companyData);
		for ($i = 0, $s = count($companyData); $i < $s; $i++) {
			$company[] = CRest::callBatch ($companyData[$i]);
		}

		### формируем массив расчетчик => компании под его контролем ###
		for ($i = 0, $s = count($company); $i < $s; $i++) {
			foreach ($company[$i]['result']['result'] as $value) {
				foreach ($value as $v) {
					$arrPersonal[$v['UF_CRM_1518701540']]['id'] = $v['ID'];
					$arrPersonal[$v['UF_CRM_1518701540']]['title'] = $v['TITLE'];
				}
			}
		}

		### ищем главбуха ###
		$accountant = CRest::call('department.get', array());
		for ($i = 0, $s = count($accountant['result']); $i < $s; $i++) {
			if ($accountant['result'][$i]['NAME'] == 'Бухгалтерия') {
				$mainAccountant = $accountant['result'][$i]['UF_HEAD'];
			}
		}

		### формируем задачи расчетчикам ###
		foreach ($arrPersonal as $k => $v) {
			$taskData[] = array(
				'method' => 'tasks.task.add',
				'params' => array(
					'fields' => array(
						'TITLE' => 'Новая задача',
						'CREATED_BY' => $mainAccountant,
						'RESPONSIBLE_ID' => $k,
						'DEADLINE' => date('Y-m-d', mktime(0, 0, 0, date('m'), 8, date('Y'))),
					)
				)
			);
		}
		if (count($taskData) > 50) $taskData = array_chunk($taskData, 50);
		else $taskData = array($taskData);
		for ($i = 0, $s = count($taskData); $i < $s; $i++) {
			$newTask[] = CRest::callBatch($taskData[$i]);
		}

		### формируем чек-листы к поставленным задачам ###
		for ($i = 0, $s = count($newTask); $i < $s; $i++) {
			for ($y = 0, $size = count($newTask[$i]['result']['result']); $y < $size; $y++) {
				foreach ($arrPersonal[$newTask[$i]['result']['result'][$y]['task']['responsibleId']] as $value) {
					$checkData[] = array(
						'method' => 'task.checklistitem.add',
						'params' => array(
							'ID' => $newTask[$i]['result']['result'][$y]['task']['id'],
							'fields' => array(
								'TITLE' => 'Компания: '.$value['title'],
								'IS_COMPLETE' => 'N'
							)
						)
					);
				}
			}
		}
		if (count($checkData) > 50) $checkData = array_chunk($checkData, 50);
		else $checkData = array($checkData);
		for ($i = 0, $s = count($checkData); $i < $s; $i++) {
			$newCheck[] = CRest::callBatch ($checkData[$i]);
		}

		file_put_contents(__DIR__.'/flag.txt', '');
		return 'repeat_deals();';
	}
}